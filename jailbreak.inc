#if defined _jailbreak_included
	#endinput
#endif
#define _jailbreak_included

#include <cstrike>
#include <sdktools>

#define JB_PREFIX "\x04[\x0Bx\x08G\x04]\x01 "
#define JB_PREFIX_NOCOLOR "[xG] "

#if defined _ccsplayer_included && !defined JB_INT_PLAYERS
	#define JBPLAYER CCSPlayer
#else
	#define JBPLAYER int
#endif

enum AbortReason
{
	Abort_Cheater = 0,
	Abort_Silent,
	Abort_PlayerWon,
	Abort_PlayerDisconnect
};

enum LR_Attempt
{
	LR_Success = 0,
	LR_NotAvailable, // Too many players alive or between round end and round start
	LR_WrongTeam,
	LR_NotAlive,
	LR_AlreadyActive,
	LR_BetweenRounds,
	LR_SuccessOverride,
	LR_FailOverride,
	LR_Continue
};

enum PassReason
{
	PR_WardenPassed = 0, // the warden used sm_pass
	PR_AdminPassed, // An admin passed the warden.
	PR_WardenDeath, // warden died
	PR_PlayerLR, // Player got LR
	PR_RoundEnd // Round Ended
};

enum RoundType
{
	RT_WardenActive = 0,
	RT_BetweenRounds,
	RT_Freeday,
	RT_NeedWarden,
	RT_LR,
};

enum LogReason
{
	Reason_Unknown = 0,
	Reason_Vent,
	Reason_Damage,
	Reason_Kill,
	Reason_Button,
	Reason_Chat
};

typeset JB_OnSelect
{
	function void (int iPrisoner, int iGuard);
	function void (int iPrisoner, int iGuard, const char[] sLrName);
	#if defined _ccsplayer_included
	function void (CCSPlayer pPrisoner, CCSPlayer pGuard);
	function void (CCSPlayer pPrisoner, CCSPlayer pGuard, const char[] sLrName);
	#endif
};

typeset JB_OnStart
{
	function void (int iPrisoner, int iGuard);
	#if defined _ccsplayer_included
	function void (CCSPlayer pPrisoner, CCSPlayer pGuard);
	#endif
}

typeset JB_OnEnd
{
	function void (int iPrisoner, int iGuard, int iWinner, AbortReason reason);
	#if defined _ccsplayer_included
	function void (CCSPlayer pPrisoner, CCSPlayer pGuard, CCSPlayer pWinner, AbortReason reason);
	#endif
}

/*
 * Creates a category for LRs to use.
 *
 * @param 		sName			Name of the category
 *
 * @noreturn
 */
native void JB_CreateCategory(const char[] sName);

/*
 * Creates a new last request
 *
 * @param		onStart			Function called when this LR starts
 * @param		onEnd			Function called when this LR ends
 * @param		sName			Display name of the LR
 * @param		sCategory		Name of the category that owns this. Set to "" if no owning category.
 * @param 		onSelect		Function called when players attempt to select this. If not INVALID_FUNCTION, you must manually call Jailbreak_StartLR()
 * @param 		bSelectGuard	Should there be a CT thats chosen for this LR. if not, you must either manually end it or it will go on until round end.
 *
 * @noreturn
 */
native void JB_CreateLR(JB_OnStart onStart, JB_OnEnd onEnd, const char[] sName, const char[] sCategory="", JB_OnSelect onSelect=INVALID_FUNCTION, bool bSelectGuard=true);

/*
 * Removes a last request from the menu by name and callback
 *
 * @param		onStart			Callback of LR to delete. Doesn't remove all LRs that use this callback.
 * @param		sName			Name of LR to delete
 *
 * @return 		true upon success, false otherwise
 */
native bool JB_DeleteLR(JB_OnStart onStart, const char[] sName);

/*
 * Forces the currently selected LR to be activated.
 * Your plugin must be the owner of the currently selected LR. Refer to JB_GetSelectedLR()
 * 
 * @return		true upon success, false upon failure.
 */
native bool JB_StartLR();

/*
 * Checks if an LR exists
 *
 * @param		sName			Name of the LR to find
 * @param		sCategory		Category that the LR belongs to. Set to "" if no category
 *
 * @return 		true if found, false otherwise.
 */
native bool JB_FindLR(const char[] sName, const char[] sCategory = "");

/*
 * Gets the currently selected LR's info
 *
 * @param		bOwner			Is this plugin the owner of this LR. Passed by reference, optional
 * @param		sName			Buffer to store the name of the LR
 * @param		iNameLen		Max size of sName
 * @param		sCategory		Buffer to store the category of the LR
 * @param		iCategoryLen	Max size of sCategory
 *
 * @return		true if an LR is selected, false otherwise
 */
native bool JB_GetSelectedLR(bool &bOwner=false, char[] sName = "", int iNameLen = 0, char[] sCategory = "", int iCategoryLen = 0);

/*
 * Deletes a category from the LR menu
 *
 * @param 		sName			Name of the category to delete
 *
 * @return 		true on success, false on failure
 */
native bool JB_DeleteCategory(const char[] sName);

/*
 * Aborts the active LR.
 *
 * @param		reason			Reason to abort.
 * @param		data			Used for PlayerWon, who won the LR.
 *
 * @return		true upon success, false upon failure (no active LR).
 */
native bool JB_AbortLR(AbortReason reason, any data = 0);

/*
 * Check if a player is in an active LR
 *
 * @param		p				Client to check
 *
 * @return 		true if player is actively in an LR, false otherwise.
 */
native bool JB_IsPlayerInLR(JBPLAYER p);

/* Check if you can safely call JB_StartLR().
 * Verifies that all checks inside JB_StartLR() will pass, given that nothing
 * changes between the time you call the two.
 *
 * @return 		true if its safe to call, false otherwise.
 */
native bool JB_CanCallStartLR();

/*
 * Adds a weapon players inside an LR can pickup.
 * If you don't add any, it assumes that all weapons are allowed.
 *
 * @param		allowedWeapon	Either a WeaponDef or WeaponGroup that is allowed.
 *
 * @noreturn
 */
native void JB_AddAllowedWeapon(any allowedWeapon);

/*
 * Removes a weapon players inside an LR can pickup.
 * If you remove all weapons, all weapons can be picked up again.
 *
 * @param		disallowedWeapon	Either a WeaponDef or WeaponGroup that is allowed.
 *
 * @noreturn
 */
native void JB_RemoveAllowedWeapon(any disallowedWeapon);

/*
 * Adds a line to the jailbreak round logs.
 *
 * @param		reason			Reason for adding to logs, used for sorting.
 * @param		sInfo			string to add to the logs
 * @param		...				formating arguments
 *
 * @noreturn
 */
native void JB_AddToLog(LogReason reason, const char[] sMessage, any ...);

/*
 * Sets the warden, assuming its enabled.
 *
 * @param 		iClient			Set the warden to this client
 *
 * @noreturn
 */
native void JB_SetWarden(JBPLAYER client);

/*
 * Removes the current warden, assuming he exists
 *
 * @noreturn
 */
native void JB_PassWarden();

/*
 * Gets the current warden
 *
 * @return 		If using ints, player index of warden, or -1 if no warden exists.
 * 				if using CCSPlayers, CCSPlayer of the warden, or NULL_CCSPLAYER if no warden exists
 */
native JBPLAYER JB_GetWarden();

/*
 * Gets the current round type
 *
 * @return 		Curent round type, look at RoundType enum for more info
 */
native RoundType JB_GetRoundType();

/*
 * Sets the current round type
 *
 * @param		type			Type to set the round to
 *
 * @noreturn
 */
native void JB_SetRoundType(RoundType type);

/*
 * Checks if a player is in the Guard queue
 *
 * @param 		client			Client to check if hes in the queue
 *
 * @return		true if in the queue, false otherwise
 */
native bool JB_InGuardQueue(JBPLAYER client);

/*
 * Gets a client's position in the guard queue
 *
 * @param		client			Client to get in the queue
 *
 * @return		position in the queue, -1 if not in queue
 */
native int JB_GetGuardPosition(JBPLAYER client);

/*
 * Gets a client's priority in the guard queue
 *
 * @param		client			Client to get in the queue
 *
 * @return		priority in the queue, -1 if not in queue
 */
native int JB_GetGuardPriority(JBPLAYER client);

/*
 * Drops a client from the queue
 *
 * @param		client			Client to drop from the queue
 *
 * @noreturn
 */
native void JB_DropFromGuardQueue(JBPLAYER client); 

/*
 * Opens all cell doors
 *
 * @noreturn
 */
native void JB_OpenCells();

/*
 * Start a freeday
 *
 * @param		bPreventBlock	Prevents JB_OnFreedayStart forward from being blocked by other plugins
 * @param		bPreventSilent	Prevents JB_OnFreedayStart forward from being set to silent by other plugins
 *
 * @noreturn
 */
native void JB_StartFreeday(bool bPreventBlock, bool bPreventSilent);
/*
 * Fires when an LR is activated
 * @param 		prisoner 		The T in the LR 
 * @param 		guard			The CT in the LR. Can be -1 if Lr doesn't specify a guard.
 * 
 * @noreturn
 */
forward void JB_OnLrActivated(JBPLAYER prisoner, JBPLAYER guard);

/*
 * Fires when an LR is finished.
 * @param 		prisoner 		The T in the LR 
 * @param 		guard			The CT in the LR. Can be -1 if Lr doesn't specify a guard.
 * @param 		data			if not interrupted, Winner of the LR. If reason is Abort_Cheater, this is the cheater. 
 * @param		reason			Reason the LR was finished. Can be because another plugin or admin interrupted it.
 * 
 * @noreturn
 */
forward void JB_OnLrEnded(JBPLAYER prisoner, JBPLAYER guard, JBPLAYER data, AbortReason reason);

/*
 * Fires when a client attempts to build the LR select menu 
 * 
 * @param		prisoner		Client who attempted to LR 
 * @param		attempt			What jailbreak decided to do with the attempt. Passed by reference, change this to override.
 * @param		sFailReason		Custom reason that the attempt failed
 * @param		iMax			Maximum length of sFailReason
 *
 * @return		Plugin_Stop to silently stop, anything else to continue as normal.
 */
forward Action JB_OnAttemptLR(JBPLAYER prisoner, LR_Attempt &attempt, char[] sFailReason, int iMax);

/*
 * Fires when a client obtains LR 
 *
 * @param		prisoner		Client who obtained LR 
 *
 * @return	 	Plugin_Continue to allow the plugin to announce it, Plugin_Stop to prevent it from being announced.
 */
forward Action JB_OnAnnouncedLR(JBPLAYER prisoner);

/*
 * Fires when a client obtains LR, fires after LR sound and chat message is played. bAnnounced to check if either was used.
 *
 * @param		prisoner		Client who obtained LR 
 * @param		bAnnounced		true if no plugins prevented announcement, false otherwise
 * 
 * @noreturn
 */
forward void JB_OnAnnouncedLrPost(JBPLAYER prisoner, bool bAnnounced);

/*
 * Fires after modifying a player's weapons after he spawns.
 * If you make changes to weapons during player_spawn, use this forward instead, or it might get overriden.
 * 
 * @param		client			Client who spawned
 *
 * @noreturn
 */
forward void JB_OnPostSpawn(JBPLAYER client);

/*
 * Fires when a warden dies.
 *
 * @param 		killer			Client who killed the warden
 * @param		warden			Client who was the warden.
 *
 * @noreturn
 */
forward void JB_OnWardenDeath(JBPLAYER killer, JBPLAYER warden);

/*
 * Fires when a warden is removed
 *
 * @param 		warden			Warden who was removed
 * @param		reason			Reason he was removed as warden
 *
 * @noreturn
 */
forward void JB_OnWardenRemoved(JBPLAYER warden, PassReason reason);

/*
 * Fires when a new warden is set
 *
 * @param		client		Client who got warden
 *
 * @noreturn
*/
forward void JB_OnWardenSet(JBPLAYER client);

/*
 * Fires when a freeday is started
 *
 * @param 		bSilent		Should this not alert players that its a freeday?
 *
 * @return Plugin_Continue to continue as normal, anything else to stop.
 * If you return anything but Plugin_Continue, bSilent is ignored. A new CT can take warden. 
 */
forward Action JB_OnFreedayStart(bool &bSilent);
/*
 * These natives aren't enabled by default. Use the natives to ensure that it can be used
 */

/*
 * Checks if teambans is enabled
 *
 */
native bool JB_TeambansEnabled();

/*
 * Checks if a player is currently teambanned
 *
 * @param		player			Client to check if is teambanned
 *
 * @return 		true if teambanned, false if not 
 * @errors 		teambans not available
 */
native bool JB_IsTeambanned(JBPLAYER player);
 
/*
 * Teambans a player.
 *
 * @param		player			client to teamban
 * @param		iDuration		Time to ban in minutes. 0 for permanent. Must be a positive number 
 * @param		reason			reason for the ban 
 *
 * @return 		true on success, false otherwise.
 * @errors		player already teambanned, teambans not available
 */
native bool JB_TeambanPlayer(JBPLAYER player, int iDuration, const char[] sReason, any ...);

/*
 * Unteambans a player.
 *
 * @param		player			client to teamban
 * @param		reason			reason for the unban 
 *
 * @return		true on success, false otherwise.
 * @errors		player not teambanned, teambans not available
 */
native bool JB_UnteambanPlayer(JBPLAYER player, const char[] sReason, any ...);

/*
 * Creates a HUD announcement, near the center-bottom of the screen.
 *
 * @param		iPrimaryColor	primary color for the message
 * @param		iSecondaryColor	secondary color for the message
 * @param		sMsg			message to display
 *
 * @return		true on success, false if unable to create
 */
native bool JB_HudAnnounce(int iPrimaryColor[4], int iSecondaryColor[4], const char[] sMsg, any ...);

/*
 * Creates a HUD persistent notification, near the top-left of the screen.
 *
 * @param		iColor			color for the message
 * @param		sMsg			message to display
 *
 * @return		integer id of the created hud notification, -1 if unable to create
 */
native int JB_HudCreateNotification(int iColor[4], const char[] sMsg, any ...);

/*
 * Creates a HUD persistent notification, near the top-left of the screen.
 *
 * @param		iId				id of the notification to remove
 *
 * @return		true on success, false if id not found
 */
native bool JB_HudRemoveNotification(int iId);

stock void JB_StripToKnife(JBPLAYER p)
{
	for(int i = 0; i <= CS_SLOT_C4;i++)
	{
		#if defined _ccsplayer_included && !defined JB_INT_PLAYERS
		CWeapon wep;
		while((wep = p.GetWeapon(i)) != NULL_CWEAPON)
		{
			p.RemoveItem(wep);
			wep.Kill();
		}
		#else
		int iWep;
		while((iWep = GetPlayerWeaponSlot(p, i)) != -1)
		{
			RemovePlayerItem(p, iWep);
			AcceptEntityInput(iWep, "Kill");
		}
		#endif
	}
	#if defined _ccsplayer_included
	GivePlayerWeapon(p, "weapon_knife");
	#else
	GivePlayerItem(p, "weapon_knife");
	#endif
}

stock void JB_FullStrip(JBPLAYER p)
{
	for(int i = 0; i <= CS_SLOT_C4;i++)
	{
		#if defined _ccsplayer_included && !defined JB_INT_PLAYERS
		CWeapon wep;
		while((wep = p.GetWeapon(i)) != NULL_CWEAPON)
		{
			p.RemoveItem(wep);
			wep.Kill();
		}
		#else
		int iWep;
		while((iWep = GetPlayerWeaponSlot(p, i)) != -1)
		{
			RemovePlayerItem(p, iWep);
			AcceptEntityInput(iWep, "Kill");
		}
		#endif
	}
}

public SharedPlugin __pl_jailbreak =
{
	name = "jailbreak",
	file = "jailbreak.smx",
	#if defined REQUIRE_PLUGIN
		required = 1,
	#else
		required = 0,
	#endif
};

#if !defined REQUIRE_PLUGIN
	public void __pl_jailbreak_SetNTVOptional()
	{
		MarkNativeAsOptional("JB_CreateLR");
		MarkNativeAsOptional("JB_CreateCategory");
		MarkNativeAsOptional("JB_StartLR");
		MarkNativeAsOptional("JB_FindLR");
		MarkNativeAsOptional("JB_DeleteCategory");
		MarkNativeAsOptional("JB_GetSelectedLR");
		MarkNativeAsOptional("JB_DeleteLR");
		MarkNativeAsOptional("JB_AbortLR");
		MarkNativeAsOptional("JB_IsPlayerInLR");
		MarkNativeAsOptional("JB_CanCallStartLR");
		MarkNativeAsOptional("JB_AddToLog");
		MarkNativeAsOptional("JB_GetWarden");
		MarkNativeAsOptional("JB_SetWarden");
		MarkNativeAsOptional("JB_PassWarden");
		MarkNativeAsOptional("JB_GetRoundType");
		MarkNativeAsOptional("JB_SetRoundType");
		MarkNativeAsOptional("JB_StartFreeday");
		MarkNativeAsOptional("JB_OpenCells");
		MarkNativeAsOptional("JB_InGuardQueue");
		MarkNativeAsOptional("JB_GetGuardPosition");
		MarkNativeAsOptional("JB_GetGuardPriority");
		MarkNativeAsOptional("JB_DropFromGuardQueue");
		MarkNativeAsOptional("JB_TeambansEnabled");
		MarkNativeAsOptional("JB_IsTeambanned");
		MarkNativeAsOptional("JB_TeambanPlayer");
		MarkNativeAsOptional("JB_UnteambanPlayer");
		MarkNativeAsOptional("JB_HudAnnounce");
		MarkNativeAsOptional("JB_HudCreatePersistent");
		MarkNativeAsOptional("JB_HudRemovePersistent");
		
	}
#endif