#include <sourcemod>
#include <ccsplayer>
#include <jailbreak>

// Creates our LRs
public void OnPluginStart()
{
	// Simplest LR, we don't have to do much here.
	JB_CreateLR(OnKnifeFightStart, OnKnifeFightEnd, "Knife Fight");
	
	// More complex LR, we will need to do cleanup on end.
	JB_CreateLR(OnRebelStart, OnRebelEnd, "Rebel", _, _, false);
}

/*
 * Called when a player selects our LR.
 *
 * @param	pPrisoner		prisoner who selected our LR 
 * @param	pGuard			guard who was selected by the prisoner
 * @noreturn
 */
public void OnKnifeFightStart(CCSPlayer pPrisoner, CCSPlayer pGuard)
{
	/*
	 * Before starting LR, the main plugin will make sure that both players
	 * have full health & armor. It will also strip both players to their knives for us.
	 * To ensure fairness, we only have to set a couple more properties.
	 */
	 
	// Reset speed
	pPrisoner.Speed = 1.0;
	pGuard.Speed = 1.0;
	
	// Reset gravity
	pPrisoner.Gravity = 1.0;
	pGuard.Gravity = 1.0;
	
	// Remove heavy armor
	pPrisoner.HeavyArmor = false;
	pGuard.HeavyArmor = false;
}

/*
 * Called when our LR is finished.
 *
 * @param	pPrisoner		prisoner who selected our LR 
 * @param	pGuard			guard who was selected by the prisoner
 * @param	pWinner			Player who won. Not always a valid player, if aborted for other reasons.
 * @param	reason			Reason the LR ended.
 * @noreturn
 */
public void OnKnifeFightEnd(CCSPlayer pPrisoner, CCSPlayer pGuard, CCSPlayer pWinner, AbortReason reason)
{
	/*
	 * When any LR is ended. the main plugin will reset armor, health, and helmet.
	 * Both players will be stripped to their knives.
	 * Since we don't have anything else to cleanup, we'll just print the reason it was aborted.
	 * For fun.
	 */
	PrintToChatAll("Our LR was aborted for the reason %d", reason);
}

/*
 * Called when a player selects our LR.
 *
 * @param	pPrisoner		prisoner who selected our LR. 
 * @param	pGuard			guard who was selected by the prisoner. 
 *							This will be invalid, since no guard will be selected.
 * @noreturn
 */
public void OnRebelStart(CCSPlayer pPrisoner, CCSPlayer pGuard)
{
	// Give the prisoner weapons
	GivePlayerWeapon(pPrisoner, "weapon_negev");
	GivePlayerWeapon(pPrisoner, "weapon_deagle");
	
	// Increase prisoner health
	pPrisoner.Health = 200;
	
	// Give heavy armor to the prisoner
	pPrisoner.HeavyArmor = true;
	
	/*
	 * Guards will not be stripped if no guard is selected.
	 * We still want to make sure everyone has weapons, so we'll give everyone
	 * rifles
	 */
	CCSPlayer p;
	while(CCSPlayer.Next(p))
	{
		if(p.InGame && p.Alive && p.Team == CS_TEAM_CT)
		{
			if(p.GetWeapon(CS_SLOT_PRIMARY).IsNull)
			{
				GivePlayerWeapon(p, "weapon_m4a1");
			}
			if(p.GetWeapon(CS_SLOT_SECONDARY).IsNull)
			{
				GivePlayerWeapon(p, "weapon_deagle");
			}
		}
	}
}

/*
 * Called when our LR is finished.
 *
 * @param	pPrisoner		prisoner who selected our LR 
 * @param	pGuard			guard who was selected by the prisoner, will be invalid
 * @param	pWinner			Player who won. Not always a valid player, if aborted for other reasons.
 * @param	reason			Reason the LR ended.
 * @noreturn
 */
public void OnRebelEnd(CCSPlayer pPrisoner, CCSPlayer pGuard, CCSPlayer pWinner, AbortReason reason)
{
	/*
	 * Again, we don't have anything that we need to cleanup. So we'll just check if the prisoner won, or the CTs.
	 */
	if(reason == Abort_PlayerWon)
	{
		if(pWinner == pPrisoner)
		{
			PrintToChatAll("The prisoner won!");
		}
		else
		{
			PrintToChatAll("The guards won!");
		}
	}
}